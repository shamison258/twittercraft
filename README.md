# TwitterCraft
## 作成動機
Twitterと連携してTwitter内のアクションをMineCraftにフィードバックさせてみたら,
面白いのではといったもの.

### 例
- ふぁぼられたらHP回復
- 1ツイート毎にMoneyゲット

## 機能

### 主要機能
- MineCraftIDとTwitterIDを連携と解除
- タイムラインの閲覧(サーバの負荷が大きくなりそうな予感)
- ツイート
- リプライ, ふぁぼの通知

### できたらいいな機能
- 経済MODとの連携
- jobシステム(fav特化, RT特化,ツイート特化)
- TwitterのステータスによってPlayerのパラメータを変化させる